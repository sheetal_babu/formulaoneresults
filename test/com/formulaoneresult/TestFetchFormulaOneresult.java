package com.formulaoneresult;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;


public class TestFetchFormulaOneresult {
	
	FetchFormulaOneResult objFetchFormulaOneResult = new FetchFormulaOneResult();
	Document doc = null;
	Properties prop = null;

	@Before
	public void testUrlSetUp() throws FormulaOneException{
		
		prop = objFetchFormulaOneResult.getPropValues("/config.properties");
		try{
			doc = Jsoup.connect(prop.getProperty("URL_DATA")).get();
		}catch (IOException e) { 
			e.printStackTrace(); 
		}
	}
	
	@Test(expected = FormulaOneException.class)
	public void testExceptionThrownIngetPropValues() throws FormulaOneException{
		objFetchFormulaOneResult.getPropValues("fff");
	}
	
	@Test
	public void testGetFormOneResult() throws FormulaOneException {
		
		FileReader file = null;
		try {
			file = new FileReader(prop.getProperty("TEST_OUT_FILE"));
		} catch (IOException e) {
			throw new FormulaOneException("Please specify proper file name.");
		}
		
		objFetchFormulaOneResult.getFormOneResult();
		FileReader outfile = null;
		try {
			outfile = new FileReader(prop.getProperty("OUTPUT_FILE_NAME"));
		} catch (IOException e) {
			throw new FormulaOneException("Please specify proper file name.");
		}
		
		assert(file.equals(outfile));
	}
	
	@Test
	public void testGetFormOneDriverRatingsPositive() throws FormulaOneException {
		
		Scanner input = null;
		try {
			input = new Scanner(new FileReader(prop.getProperty("DRIVER_INPUT_FILE")));
		} catch (FileNotFoundException e) {
			throw new FormulaOneException("Please specify proper file name.");
		}
		JSONArray arrTestJSON = new JSONArray();
		while(input.hasNext()){
			JSONObject jo = new JSONObject();
			String key = input.nextLine();
		    String value = input.nextLine();
		    jo.put("DRIVER_NAME", key);
		    jo.put("POINTS", value);
		    arrTestJSON.add(jo);
		}
		assertEquals(arrTestJSON, objFetchFormulaOneResult.getFormOneDriverRatings(doc));
	}
	
	@Test
	public void testGetFormOneTeamRatingsPositive() throws FormulaOneException {
		
		Scanner input = null;
		try {
			input = new Scanner(new FileReader(prop.getProperty("TEAM_INPUT_FILE")));
		} catch (FileNotFoundException e) {
			throw new FormulaOneException("Please specify proper file name.");
		}
		JSONArray arrTestJSON = new JSONArray();
		while(input.hasNext()){
			JSONObject jo = new JSONObject();
			String key = input.nextLine();
		   // String value = input.nextLine();
		    jo.put("TEAM_NAME", key);
		   // jo.put("POINTS", value);
		    arrTestJSON.add(jo);
		}
		assertEquals(arrTestJSON, objFetchFormulaOneResult.getFormOneTeamRatings(doc));
	}
	
	@Test
	public void testGetSortedArray() throws FormulaOneException{
		
		Scanner input = null;
		try {
			input = new Scanner(new FileReader(prop.getProperty("SORT_INPUT_FILE")));
		} catch (FileNotFoundException e) {
			throw new FormulaOneException("Please specify proper file name.");
		}
		JSONArray arrTestSortJSON = new JSONArray();
		while(input.hasNext()){
			JSONObject jo = new JSONObject();
			String key = input.nextLine();
		    String value = input.nextLine();
		    jo.put("TEAM_NAME", key);
		    jo.put("POINTS", value);
		    arrTestSortJSON.add(jo);
		}
		try {
			input = new Scanner(new FileReader(prop.getProperty("UNSORT_INPUT_FILE")));
		} catch (FileNotFoundException e) {
			throw new FormulaOneException("Please specify proper file name.");
		}
		JSONArray arrTestUnSortJSON = new JSONArray();
		while(input.hasNext()){
			JSONObject jo = new JSONObject();
			String key = input.nextLine();
		    String value = input.nextLine();
		    jo.put("TEAM_NAME", key);
		    jo.put("POINTS", value);
		    arrTestUnSortJSON.add(jo);
		}
		assertEquals(arrTestSortJSON, objFetchFormulaOneResult.getSortedArray(arrTestUnSortJSON, 5));
		
	}

}
