package com.formulaoneresult;

import java.util.Comparator;

import org.json.simple.JSONObject;

public class MyJSONComparator implements Comparator<JSONObject> {

	@Override
	public int compare(JSONObject o1, JSONObject o2) {
		int v1 = Integer.parseInt((String)o1.get("POINTS"));
	    int v3 = Integer.parseInt((String)o2.get("POINTS"));
	    return -Integer.compare(v1, v3);
	}

}
