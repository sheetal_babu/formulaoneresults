package com.formulaoneresult;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class FetchFormulaOneResult {
	
	
	/**
	 * This method will read the data available in the url specified and 
	 * print the driver and team results onto a file
	 * @throws FormulaOneException 
	 */
	public void getFormOneResult() throws FormulaOneException{
		
		
		Properties prop = getPropValues("/config.properties");
		Document doc = null; 
		try{
			doc = Jsoup.connect(prop.getProperty("URL_DATA")).get();
		}catch (IOException e) { 
			throw new FormulaOneException("Please enter proper url."); 
		}
		JSONObject objResult = new JSONObject();
		objResult.put("Driver Results", getFormOneDriverRatings(doc));
		objResult.put("Team Results", getFormOneTeamRatings(doc));
		
		try (FileWriter file = new FileWriter(prop.getProperty("OUTPUT_FILE_NAME"))) {
			file.write(objResult.toJSONString());
		}catch (IOException e) {
			throw new FormulaOneException("The file is not proper.");
		}
		
	}
	
	/**
	 * This method takes the Document object from the url as input
	 * and fetches the driver names and points
	 * and returns a sorted list of driver names and points
	 * @param doc
	 * @return
	 */
	public JSONArray getFormOneDriverRatings(Document doc){
				
		JSONArray arrJSON = new JSONArray();
		Elements eleDriverRatings = doc.getElementsByTag("h3");
		Element tblDriverRatings = doc.select("table").after(eleDriverRatings.toString()).last();
		
		Elements ttlDriver = tblDriverRatings.select("a[title]");
		Elements elePoints = tblDriverRatings.getElementsByClass("msr_col11").select("td");
		
		for (int i = 0; i < ttlDriver.size(); i++) {
			JSONObject jo = new JSONObject();
		    String key = ttlDriver.get(i).text();
		    String value = elePoints.get(i).text();
		    jo.put("DRIVER_NAME", key);
		    jo.put("POINTS", value);
		    arrJSON.add(jo);
		    
		}
		
		JSONArray sortedDriverArr = getSortedArray(arrJSON, 10);
					
		return sortedDriverArr;
	}
	
	/**
	 * This method takes the Document object from the url as input
	 * and fetches the team names and points
	 * and returns a sorted list of team names and points
	 * @param doc
	 * @return
	 */
	public JSONArray getFormOneTeamRatings(Document doc){
				
		Elements eleTeamRatings = doc.getElementsContainingText("Championship Constructor Standings");
		//System.out.println("eleTeamRatings==="+eleTeamRatings.toString());
		String[] strArr = StringUtils.substringsBetween(eleTeamRatings.toString(), "&lt;table", "&lt;/table&gt;");
		String strIntermed = strArr[0].replaceAll("&lt;", "<");
		String strFinal = strIntermed.replaceAll("&gt;", ">");
		
		Document teamdoc = Jsoup.parse(strFinal);
		
		Elements eleTeamPoints = eleTeamRatings.select("td.msr_total");
		Elements tblTeamRatings = teamdoc.select("a[href*=f1-fansite.com/f1-teams/");
		Elements ttlTeamRatings= tblTeamRatings.select("a[title]");
		
		//System.out.println("dgdg==="+teamdoc.toString());
		//System.out.println("dgdg"+eleTeamPoints.toString());
		
		Elements ttlTeam = eleTeamRatings.tagName("a");
		
		JSONArray arrTeamRatings = new JSONArray();
		
		for (int i = 0; i < ttlTeamRatings.size(); i++) {
			JSONObject jo = new JSONObject();
		    String key = ttlTeamRatings.get(i).text();
		   // System.out.println("key==="+key);
		   // String value = eleTeamPoints.get(i).text();
		// System.out.println("value==="+value);
		    jo.put("TEAM_NAME", key);
		   // jo.put("POINTS", key);
		    arrTeamRatings.add(jo);
		    
		}
		JSONArray sortedTeamArr = new JSONArray();
		int iArrLen = arrTeamRatings.size();
		int iResultLen = 5;
        if (iArrLen<5){
        	iResultLen=iArrLen;
        }
        for(int j=0; j<iResultLen;j++){
        	sortedTeamArr.add((JSONObject) arrTeamRatings.get(j));
        }
		
		//JSONArray sortedTeamArr = getSortedArray(arrTeamRatings, 5);
		      			
		return sortedTeamArr;
	}
	
	public JSONArray getSortedArray(JSONArray arrJSON, int iResultLen){
		
		ArrayList<JSONObject> lstResults = new ArrayList<>();
		for (int i = 0; i < arrJSON.size(); i++) {
			lstResults.add((JSONObject) arrJSON.get(i));
        }
        Collections.sort(lstResults, new MyJSONComparator());
        JSONArray sortedArr = new JSONArray();
        
        int iArrLen = arrJSON.size();
        if (iArrLen<iResultLen){
        	iResultLen=iArrLen;
        }
        for(int j=0; j<iResultLen;j++){
        	sortedArr.add(lstResults.get(j));
        }
        
        return sortedArr;
	}
	
	public Properties getPropValues(String strFileName) throws FormulaOneException{
		
		Properties prop = new Properties();
		
			try {
				prop.load(this.getClass().getResourceAsStream(strFileName));
			} catch (Exception e) {
				throw new FormulaOneException("Please specify proper file name.");
			}
		
		
		//String strValue = prop.getProperty(strKey);
		//return strValue ;	
		return prop;
	}
}
