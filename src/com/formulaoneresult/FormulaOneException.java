package com.formulaoneresult;

public class FormulaOneException extends Exception {
	
	public FormulaOneException(){
		
	}
	
	public FormulaOneException(String message){
		
		//System.out.println("Formula One Exception : "+message);
		super(message);
	}
	
	public FormulaOneException(Throwable cause)
	{
		super(cause);

	}

	public FormulaOneException(String message, Throwable cause)
	{
		super(message, cause);

	}

	public FormulaOneException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
