# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This application will fetch the top 10 driver names and top 5 team names in formula one race from the below website
* http://www.f1-fansite.com/f1-results/2015-f1-results-standings/
* The result will be a JSONObject printed onto a file.


### How do I get set up? ###

* The output file name and the website details are configurable in the config.properties available in resources folder.
* Also the test input files can be specified in the config.properties.
* To run the application execute the FormulaOneResults.jar available in the repository. 
* Have used external libraries jsoup, json and apache-commons-lang
* The Junit Test case TestFetchFormulaOneResults can run to execute the testcases.